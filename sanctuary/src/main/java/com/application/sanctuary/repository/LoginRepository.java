package com.application.sanctuary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.application.sanctuary.pojo.LoginDetails;

@Repository
public interface LoginRepository extends JpaRepository<LoginDetails, Long> {

}
