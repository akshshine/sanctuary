package com.application.sanctuary.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.application.sanctuary.pojo.LoginDetails;
import com.application.sanctuary.service.LoginService;

@Controller
public class BaseController {

	@Autowired
	private LoginService loginService;

	@RequestMapping("/")
	public String home() {
		return "index";
	}

	@GetMapping("/getAll")
	public ResponseEntity<List<LoginDetails>> getAll() {
		try {
			List<LoginDetails> loginDetailslist = new ArrayList<LoginDetails>();
			loginDetailslist.addAll(loginService.getAll());
			if (loginDetailslist.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(loginDetailslist, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/getById/{id}")
	public ResponseEntity<LoginDetails> getById(@PathVariable(name = "id") Long id) {
		try {
			LoginDetails loginDetails = loginService.getLoginDetail(id);
			if (loginDetails == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(loginDetails, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/saveLoginDetails")
	public ResponseEntity<LoginDetails> saveLoginDetails(@RequestBody LoginDetails loginDetails) {
		try {
			LoginDetails loginDetailsDb = loginService.saveLoginDetails(loginDetails);
			if (loginDetailsDb == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(loginDetailsDb, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
