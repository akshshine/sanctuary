package com.application.sanctuary.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.application.sanctuary.pojo.LoginDetails;
import com.application.sanctuary.repository.LoginRepository;
import com.application.sanctuary.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private LoginRepository loginRepository;

	@Override
	public List<LoginDetails> getAll() {
		List<LoginDetails> loginDetailList = loginRepository.findAll();
		if (!loginDetailList.isEmpty())
			return loginDetailList;
		else
			return null;
	}

	@Override
	public LoginDetails getLoginDetail(Long userId) {
		if (userId != null) {
			LoginDetails loginDetails = loginRepository.findById(userId).get();
			if (loginDetails != null)
				return loginDetails;
			else
				return null;
		}
		return null;
	}

	@Override
	public LoginDetails saveLoginDetails(LoginDetails loginDetails) {
		if (loginDetails != null) {
			return loginRepository.save(loginDetails);
		}
		return null;
	}

}
