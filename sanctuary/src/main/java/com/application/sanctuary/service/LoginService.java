package com.application.sanctuary.service;

import java.util.List;

import com.application.sanctuary.pojo.LoginDetails;

public interface LoginService {

	public List<LoginDetails> getAll();

	public LoginDetails getLoginDetail(Long userId);

	public LoginDetails saveLoginDetails(LoginDetails loginDetails);

}
